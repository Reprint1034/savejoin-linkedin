{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Main (main) where

--import Lib
import qualified Data.Text                                  as T
import           Data.Text.Encoding
import qualified Data.ByteString.Lazy                       as B
import           Data.List               (find)
import           Data.Maybe              (mapMaybe)
import           Text.HTML.TagSoup
import           Text.HTML.TagSoup.Match
import           Text.StringLike         (fromString)
import           Network.Connection      (TLSSettings (..))
import           Network.HTTP.Conduit
import           Data.Aeson
import           GHC.Generics
import           System.Environment
--import           Text.Pretty.Simple      (pPrint)
--import           Debug.Trace


main :: IO ()
main = do
  args <- getArgs
  parsedDiv <- parseDivFile $ head args
  jobInfos <- allGetJobInfo $ getURLs parsedDiv
  B.putStr $ encode jobInfos


parseDivFile :: FilePath -> IO [Tag T.Text]
parseDivFile file = parseTags <$> fromString <$> readFile file


data JobInfo = JobInfo
  { url            :: T.Text
  , title          :: T.Text
  , company        :: T.Text
  , location       :: T.Text
  , description    :: T.Text
  , experience     :: T.Text
  , employmentType :: T.Text
  , jobFunction    :: T.Text
  , industries     :: T.Text
  } deriving (Generic, Eq, Show)

instance ToJSON JobInfo


-- < Get All Job Info
allGetJobInfo :: [T.Text] -> IO [JobInfo]
allGetJobInfo = mapM getJobInfo

getJobInfo :: T.Text -> IO JobInfo
getJobInfo url = do
  request <- parseURLRequest url
  return $ JobInfo url
                   (getTitleInfo request)
                   (head $ getCompanyLocationInfo request)
                   (last $ getCompanyLocationInfo request)
                   (getDescriptionInfo request)
                   (head $ getBottomInfo request)
                   (head $ drop 1 $ getBottomInfo request)
                   (head $ drop 2 $ getBottomInfo request)
                   (last $ getBottomInfo request)
-- >


-- < Get Individual Job Info
getTitleInfo :: [Tag T.Text] -> T.Text
getTitleInfo req = T.unwords [x | TagText x <- (getTitleInfoDiv req)]

getTitleInfoDiv :: [Tag T.Text] -> [Tag T.Text]
getTitleInfoDiv =
  getTagContent "h1"
    (any (== ("class"
             , "top-card-layout__title font-sans text-lg papabear:text-xl font-bold leading-open text-color-text mb-0 topcard__title"
             )))

getCompanyLocationInfo :: [Tag T.Text] -> [T.Text]
getCompanyLocationInfo req = getCleanText getCompanyLocationInfoDiv req

getCompanyLocationInfoDiv :: [Tag T.Text] -> [Tag T.Text]
getCompanyLocationInfoDiv =
  getTagContent "div" (any (== ("class" , "topcard__flavor-row")))

getDescriptionInfo :: [Tag T.Text] -> T.Text
getDescriptionInfo req = T.unlines
                         $ init
                         $ init
                         $ getCleanText getDescriptionInfoDiv req

getDescriptionInfoDiv :: [Tag T.Text] -> [Tag T.Text]
getDescriptionInfoDiv =
  getTagContent "section" (any (== ("class" , "show-more-less-html")))

getBottomInfo :: [Tag T.Text] -> [T.Text]
getBottomInfo req =
  filter (\t -> not $ or [ t == "Seniority level"
                         , t == "Employment type"
                         , t == "Job function"
                         , t == "Industries"
                         ]) $ getCleanText getBottomInfoDiv req

getBottomInfoDiv :: [Tag T.Text] -> [Tag T.Text]
getBottomInfoDiv =
  getTagContent "ul" (any (== ("class" , "description__job-criteria-list")))

getCleanText :: ([Tag T.Text] -> [Tag T.Text]) -> [Tag T.Text] -> [T.Text]
getCleanText func req = filter (not . T.null)
                        $ map T.strip [x | TagText x <- (func req)]
-- >


-- < Fetch URL Link Request
parseURLRequest :: T.Text -> IO [Tag T.Text]
parseURLRequest url = do
  rawRequest <- getURLRequest url
  return $ parseTags rawRequest

getURLRequest :: T.Text -> IO T.Text
getURLRequest url = do
  requestRaw <- parseRequest $ T.unpack url
  let request = requestRaw
        {requestHeaders = [("User-Agent", "curl/8.1.1")
                          ]}
  let settings =
        mkManagerSettings (TLSSettingsSimple True False False) Nothing
  manager <- newManager settings
  res <- httpLbs request manager
  return $ decodeUtf8 $ B.toStrict $ responseBody res
-- >


-- < Get URL Links
getURLs :: [Tag T.Text] -> [T.Text]
getURLs = getURLLinks . getURLTags . getURLUl

getURLLinks :: [Tag T.Text] -> [T.Text]
getURLLinks tags = map (T.takeWhile (/='?')) rawURLs
  where
    rawURLs = mapMaybe (fmap snd . find (\(t,_) -> t == "href")) attrs
    attrs = [x | TagOpen "a" x <- tags]

getURLTags :: [Tag T.Text] -> [Tag T.Text]
getURLTags = filter (tagOpen (=="a") (any (==("class", "app-aware-link "))))

getURLUl :: [Tag T.Text] -> [Tag T.Text]
getURLUl =
  getTagContent "ul"
    (any (== ("class"
             , "reusable-search__entity-result-list list-style-none"
             )))
-- >
