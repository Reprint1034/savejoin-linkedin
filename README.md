# savejoin-linkedin (Work in Progress)

Tool to export LinkedIn's saved jobs page into JSON format.

## How to use (current commit on main branch)
1. Open your LinkedIn saved jobs page (https://www.linkedin.com/my-items/saved-jobs/)
2. Open developer tools (CTRL+SHIFT+I in Firefox)
3. Copy the elements inside the body tag into a file
4. Open the directory of this repo and run the following:
``
stack exec savejoin-linkedin-exe <FILEPATH>
``

## Todo
1. Implement sign-in functionality directly
2. Export all saved jobs from all pages (requires sign-in functionality)
